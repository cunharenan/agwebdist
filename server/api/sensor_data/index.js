'use strict';

var express = require('express');
var controller = require('./sensor_data.controller');

var router = express.Router();

router.get('/', controller.index);
router.get('/last_by_sensor/:id', controller.lastBySensor);
router.get('/view', controller.view);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.post('/insert_by_sensor', controller.insertBySensor);
router.post('/insert_multiple_by_sensor', controller.insertMultipleBySensor);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);

module.exports = router;